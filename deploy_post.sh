#!/bin/bash

ENVIRONMENT=$1
MFILE=$2

echo "$ENVIRONMENT"
echo "$MFILE"

rm -rf maria-backup
tar -zxf "$MFILE"
rm -rf "$MFILE"
mv maria-backup/.env."$ENVIRONMENT" maria-backup/.env
ls -lh

exit
