#!/bin/bash

ENVIRONMENT=$1
MFILE='deploy.maria-backup.tgz'

if [[ "$ENVIRONMENT" = 'qa' ]]; then
  SSH_USER='cdbackup'
elif [[ "$ENVIRONMENT" = 'staging' ]]; then
  SSH_USER='db2-prod'
else
  exit
fi

echo "$ENVIRONMENT"

POST_SCRIPT='deploy_post.sh'

tar --exclude=.git --exclude=.gitignore --exclude=deploy* --exclude=*.tgz -zcf "$MFILE" ../maria-backup
scp "$MFILE" "$SSH_USER":.
rm "$MFILE"

ssh -tt "$SSH_USER" 'bash -s' < "$POST_SCRIPT" "$ENVIRONMENT" "$MFILE"
exit
