from time import time
from datetime import datetime
from shutil import copytree, ignore_patterns
import sys
import os
import traceback
from config.config import config
from config.helpers import get_total_time

start_time = time()
target_dir = ''

try:
    # suspend the cron job
    print("suspending cron...")
    os.system("mv /var/spool/cron/root /var/spool/cron/root.disabled")

    now = datetime.now()
    timestamp = now.strftime("%b_%d_%Y__%H_%M_%S")

    # get the environment script is running on to index configuration
    try:
        env = sys.argv[1]
        conf = config[env]
        production_copy_folder = conf['production_copy_folder'] + '.' + timestamp
        production_data_folder = conf['production_data_folder']
    except IndexError:
        raise SystemExit("You must supply the environment name (qa, staging, prod)")

    # get the name of the backup folder
    try:
        directory_name = sys.argv[2]
    except IndexError:
        raise SystemExit("You must supply the backup directory to use (backup__Jan_01_2020__01_01_01)")

    # create a backup copy of the existing datetime
    print("Backup existing production data...")
    destination = copytree(production_data_folder, production_copy_folder, ignore=ignore_patterns('mysql.sock'))

    # prepare the backup
    print("Preparing the data...")
    target_dir = f"{conf['backup-dir']}/{directory_name}/"
    os.system(f"mariabackup --prepare --target-dir={target_dir}")

    # stop mariadb
    print("Stopping mariadb, deleting prod data...")
    os.system("systemctl stop mariadb.service")
    os.system(f"rm -rf {production_data_folder}/*")

    # now perform the backup
    print("Restoring the data...")
    restore_prepare = os.system(f"mariabackup --copy-back --target-dir={target_dir}")

    # fix permissions just in case
    print("Updating permissions...")
    os.system(f"chown -R mysql:mysql {production_data_folder}")

    # restart mariadb
    print("Restarting mariadb...")
    os.system(f"systemctl start mariadb.service")

    # restart cron
    print("restarting cron")
    os.system("mv /var/spool/cron/root.disabled /var/spool/cron/root")
except IndexError as i:
    print("An IndexError occurred")
    print(i)
    traceback.print_exc()
except KeyError as k:
    print("A KeyError occurred")
    print(k)
    traceback.print_exc()
except Exception as e:
    print("An exception occurred")
    print(e)
    traceback.print_exc()
finally:
    end_time = time()
    print(f"----- time to completion: {get_total_time(start_time, end_time)}")
    print(f"Your restoration has been created from '{target_dir}' directory")


