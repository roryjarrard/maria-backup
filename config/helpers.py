def get_total_time(start_time, end_time) -> str:
    overall_seconds = end_time - start_time
    if overall_seconds < 60:
        return f"{overall_seconds} seconds"
    if overall_seconds >= 60:
        minutes_component = int(overall_seconds // 60)
        seconds_component = float(overall_seconds % 60)
        return f"{minutes_component} minutes {seconds_component} seconds"
