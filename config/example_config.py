config = {
    'windows-server': {
        'backup-dir': r'C:\Users\someuser\backups',  # r-string for safe windows paths
        'user': 'db_user',  # database user
        'password': 'db_pass',  # database password
        'remove_index': 8,  # max number of backups to keep
        'production_copy_folder': r'C:\Users\someuser\backups\production-copy',  # copy data dir here before restoring backup
        'production_file_folder': r'C:\Program Files\MariaDB 10.5\data',  # location of mysql data e.g. /var/lib/mysql
        'databases-exclude': ''  # nothing or space-separated list e.g. "data1 data2 data3.some_table"
    },
    'staging': {
        'backup-dir': '/root/backups-folder',  
        'user': 'db_user',  # database user
        'password': 'db_pass',  # database password
        'remove_index': 8,  # max number of backups to keep
        'production_copy_folder': '/root/backups-folder/production-copy',  # copy data dir here before restoring backup 
        'production_file_folder': '/var/lib/mysql',  # location of mysql data e.g. /var/lib/mysql
        'databases-exclude': ''  # nothing or space-separated list e.g. "data1 data2 data3.some_table"
    }
}
