from time import time
from datetime import datetime
import sys
import os
import traceback
from config.config import config
from config.helpers import get_total_time


def cleanup(folder: str, index: int) -> None:
    # Remove oldest backups
    command = f"cd {folder} && ls -t --ignore='*.lo[cg]' | tail -n +{index} | sudo xargs rm -rf --"
    status = os.system(command)

    # Remove oldest log files
    command = f"cd {folder} && ls -t *.log | tail -n +{index} | sudo xargs rm -rf --"
    status = os.system(command)


start_time = time()
target_dir = ''
lock_file = ''

try:

    try:
        env = sys.argv[1]
        conf = config[env]
    except IndexError:
        raise SystemExit("You must supply the environment name (qa, staging, prod)")

    now = datetime.now()
    timestamp = now.strftime("%b_%d_%Y__%H_%M_%S")

    lock_file = conf['backup-dir'] + '/backup.lock'
    if os.path.exists(lock_file):
        exit()
    else:
        f = open(lock_file, 'w')
        f.write('locking process with timestamp ' + timestamp)
        f.close()

    # clean up old logs and backups
    cleanup(conf['backup-dir'], conf['remove_index'])

    target_dir = f"{conf['backup-dir']}/backup__{timestamp}/"

    cmd_str = f"mariabackup --backup --no-lock --user={conf['user']} " \
              f"--password={conf['password']} --target-dir={target_dir} " \
              f" --databases-exclude='{conf['databases-exclude']}'"
    print("executing command\n", cmd_str)
    os.system(cmd_str)
except IndexError as i:
    print("An IndexError occurred")
    print(i)
    traceback.print_exc()
except KeyError as k:
    print("A KeyError occurred")
    print(k)
    traceback.print_exc()
except Exception as e:
    print("An exception occurred")
    print(e)
    traceback.print_exc()
finally:
    os.remove(lock_file)
    end_time = time()
    print(f"Your backups have been created in '{target_dir}' directory")
    print(f"Time to completion: {get_total_time(start_time, end_time)}\n")
